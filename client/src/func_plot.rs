use crate::DrawResult;
use plotters::prelude::*;
use plotters_canvas::CanvasBackend;
use crate::dbclient::Entry;

fn date_from_hours(d: &chrono::NaiveDateTime, h: &f32) -> String {
    let mut sum :chrono::NaiveDateTime = d.clone();
    let h_i64 : i64 = *h as i64;
    sum = sum + chrono::Duration::hours(h_i64);
    format!("{}", sum).to_string()
}

/// Draw power function f(x) = x^power.
pub async fn draw(canvas_id: &str, points: & Vec<Entry>) -> DrawResult<impl Fn((i32, i32)) -> Option<(f32, f32)>> {
    let backend = CanvasBackend::new(canvas_id).expect("cannot find canvas");
    let root = backend.into_drawing_area();

    let dur_max:f32 = (points[points.len() -1].t - points[0].t).num_hours() as f32;
    let durations = points.iter().map(|p| (( p.t - points[0].t).num_hours() as f32, p.kg as f32 ) );
    let durations_copy = durations.clone();

    let mut kgmax: f32 = points[0].kg;
    let mut kgmin: f32 = points[0].kg;

    for entry in points {
        if entry.kg > kgmax { kgmax = entry.kg; }
        if entry.kg < kgmin { kgmin = entry.kg; }
    }
    kgmax = kgmax + 1f32;
    kgmin = kgmin - 1f32;

    root.fill(&WHITE)?;

    let mut chart = ChartBuilder::on(&root)
        .margin(20u32)
        .set_label_area_size(LabelAreaPosition::Left, 40)
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .build_cartesian_2d(0f32..dur_max, kgmin..kgmax,).unwrap();

    chart.configure_mesh()
        .x_labels(3)
        .y_labels(5)
        .x_label_formatter(&|x| date_from_hours(&points[0].t,x)).draw()?;

    chart.draw_series(LineSeries::new(
        durations
        .map(|(t,k)| (t, k)),
        &RED,
    ))?;

    chart.draw_series(
        durations_copy
        .map(|(t,k)| Circle::new((t, k), 3, BLUE.filled())),
    )?;

    root.present()?;
    return Ok(chart.into_coord_trans());
}
