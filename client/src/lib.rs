use wasm_bindgen::prelude::*;

mod func_plot;
mod dbclient;

use crate::dbclient::{Entry, get_points};

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

/// Type alias for the result of a drawing function.
pub type DrawResult<T> = Result<T, Box<dyn std::error::Error>>;

/// Type used on the JS side to convert screen coordinates to chart
/// coordinates.
#[wasm_bindgen]
pub struct Chart {
    convert: Box<dyn Fn((i32, i32)) -> Option<(f64, f64)>>,
}

/// Result of screen to chart coordinates conversion.
#[wasm_bindgen]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

#[wasm_bindgen]
impl Chart {
    /// Draw provided power function on the canvas element using it's id.
    /// Return `Chart` struct suitable for coordinate conversion.
    pub async fn draw_points(canvas_id: &str) -> Result<Chart, JsValue> {

        let mut points: Vec<Entry> = get_points().await;
        points.sort_by_key(|k| k.t);

        dbclient::update_table(&points);

        let map_coord = func_plot::draw(canvas_id, &points).await.map_err(|err| err.to_string())?;
        Ok(Chart {
            convert: Box::new(move |coord| map_coord(coord).map(|(x, y)| (x.into(), y.into()))),
            })
    }

    pub async fn add_point_wrapper(date_s: &str, kg_s: &str) -> Result<JsValue, JsValue>{
        let t = chrono::NaiveDateTime::parse_from_str(&date_s, "%Y-%m-%dT%H:%M").unwrap();
        let k = kg_s.parse::<f32>().unwrap();
        dbclient::add_del_point(t, k).await
    }

    pub async fn del_point_wrapper(id: &str) -> Result<JsValue, JsValue>{
        let id_int = id.parse::<i32>().unwrap();
        dbclient::req_del_point(id_int).await
    }

    /// This function can be used to convert screen coordinates to
    /// chart coordinates.
    pub fn coord(&self, x: i32, y: i32) -> Option<Point> {
        (self.convert)((x, y)).map(|(x, y)| Point { x, y })
    }
}






