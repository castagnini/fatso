
use serde::Serialize;
use serde::Deserialize;
use wasm_bindgen::prelude::*;
/*
#[wasm_bindgen]
extern "C" {
        // Use `js_namespace` here to bind `console.log(..)` instead of just
            // `log(..)`
                #[wasm_bindgen(js_namespace = console)]
                    fn log(s: &str);
}
macro_rules! console_log {
        // Note that this is using the `log` function imported above during
            // `bare_bones`
                ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}
*/

#[derive(Serialize, Deserialize)]
pub struct NewEntry {
    pub t:   chrono::NaiveDateTime, 
    pub kg: f32
}

#[derive(Debug,Serialize, Deserialize)]
pub struct Entry {
    pub id: i32,
    pub t:   chrono::NaiveDateTime, 
    pub kg: f32
}

#[derive(Debug,Serialize,Deserialize)]
struct JsonApiResponse {
    data: Vec<Entry>,
}

pub async fn get_points() -> Vec<Entry>{
    let res = reqwest::Client::new()
        .get("https://castagnini.org:9969/entries")
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .send().await.unwrap();

    let text = res.text().await.unwrap();
    let json_res: JsonApiResponse = serde_json::from_str(&text).unwrap(); 

    // console_log!("{:#?}", todos);

    json_res.data
}

pub async fn req_del_point(id: i32) -> Result<JsValue, JsValue>{
    let url = format!("https://castagnini.org:9969/del/{}",id);
    reqwest::Client::new()
        .delete(url)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .send().await?;

    Ok( JsValue::TRUE )
}

pub async fn add_del_point(t: chrono::NaiveDateTime, k: f32) -> Result<JsValue, JsValue>{
    let url = format!("https://castagnini.org:9969/add");
    let p = NewEntry {
        t: t,
        kg: k,
    };
    reqwest::Client::new()
        .post(url).json(&p)
        .header("Access-Control-Allow-Origin", "*")
        .header("Access-Control-Allow-Methods", "*")
        .header("Access-Control-Allow-Headers", "*")
        .send().await?;

    Ok( JsValue::TRUE )
}


pub fn update_table(points: & Vec<Entry>) {

    let trash_svg = "<svg width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\"> \
  <path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/> \
    <path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/> \
    </svg>";   
    let mut inner:String = "<table class=\"table table-striped table-bordered table-fit w-auto\"><tr><th scope=\"col\">Date</th><th scope=\"col\">Weight(Kg)</th><th scope=\"col\"></th></tr>".to_string();
    for entry in points.iter().rev() {
        inner += format!("<tr><td>{}</td><td>{}</td><td><button class=\"btn btn-danger\" id=\"{}\" onClick=\"remove_point(this.id)\">{}</button></td></tr>", 
        entry.t, entry.kg, entry.id, trash_svg).as_str();
    }
    inner += "</table>";

    let window = web_sys::window().expect("should have a window in this context");
    let document = window.document().expect("window should have a document");
    document.get_element_by_id("kgtab")
        .expect("REASON!!!")
        .set_inner_html(inner.as_str());

}



