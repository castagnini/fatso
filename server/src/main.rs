
pub mod schema;
pub mod db;

#[macro_use] extern crate serde;

#[macro_use] extern crate rocket;
use db::{create_entry, query_entries, delete_entry, update_entry, establish_connection, NewEntry, Entry};
use rocket::serde::json::Json;
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::{Request, Response};
use openbsd::unveil;
use openbsd::pledge;

#[derive(Serialize)]
struct JsonApiResponse {
    data: Vec<Entry>,
}

#[get("/entries")]
fn entries_get() -> Json<JsonApiResponse> {
    let mut response = JsonApiResponse { data: vec![], };

    let mut conn = establish_connection();
    for entry in query_entries(&mut conn) {
        response.data.push(entry);
    }

    Json(response)
}
/// Catches all OPTION requests in order to get the CORS related Fairing triggered.
#[options("/<_..>")]
fn all_options() {
    /* Intentionally left empty */
}

#[post("/add", data = "<json_new_entry>")]
fn entry_add(json_new_entry: Json<NewEntry>) -> Json<bool> {
    let t = json_new_entry.0.t;
    let k = json_new_entry.0.kg;
    let mut conn = establish_connection();
    create_entry(&mut conn, t, k);

    Json(true) 
}

#[delete("/del/<id>")]
fn entry_del(id: i32) -> Json<bool> {
    let mut conn = establish_connection();
    delete_entry(&mut conn, id);

    Json(true) 
}

#[post("/edit/<id>", data = "<json_new_entry>")]
fn entry_edit(id: i32, json_new_entry: Json<NewEntry>) -> Json<bool> {
    let t = json_new_entry.0.t;
    let k = json_new_entry.0.kg;
    let mut conn = establish_connection();
    update_entry(&mut conn, id, t, k);

    Json(true) 
}

pub struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "Cross-Origin-Resource-Sharing Fairing",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, PATCH, PUT, DELETE, HEAD, OPTIONS, GET",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}


#[launch]
fn rocket() -> _ {
    _ = unveil("/etc/ssl/castagnini.org.fullchain.pem", "r");
    _ = unveil("/etc/ssl/private/castagnini.org.key", "r");
    _ = unveil("./Rocket.toml", "r");
    _ = unveil("/dev/urandom", "r");
    _ = unveil("./fatsodb.sqlite3-wal", "rwc");
    _ = unveil("./fatsodb.sqlite3-journal", "rwc");
    _ = unveil("./fatsodb.sqlite3", "rwc");
    unveil::disable();
    _ = pledge("stdio recfd ", "");
    rocket::build().attach(Cors)
    .mount("/", routes![all_options])
    .mount("/", routes![entries_get])
    .mount("/", routes![entry_add])
    .mount("/", routes![entry_del])
    .mount("/", routes![entry_edit]) 
}


