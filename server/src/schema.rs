// @generated automatically by Diesel CLI.

diesel::table! {
    mykgs (id) {
        id -> Integer,
        t -> Timestamp,
        kg -> Float,
    }
}
