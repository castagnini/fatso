
use diesel::{prelude::*, sqlite::SqliteConnection};
use crate::schema::mykgs;
use serde::Serialize;
use serde::Deserialize;

#[derive(Insertable, Serialize, Deserialize)]
#[diesel(table_name = mykgs)]
pub struct NewEntry {
    pub t:   chrono::NaiveDateTime, 
    pub kg: f32
}

#[derive(Queryable, Serialize, Deserialize)]
pub struct Entry {
    pub id: i32,
    pub t:   chrono::NaiveDateTime, 
    pub kg: f32
}

pub fn establish_connection() -> SqliteConnection {
    let db = "./fatsodb.sqlite3";
    SqliteConnection::establish(db)
        .unwrap_or_else(|_| panic!("Error connecting to {}", db))
}

pub fn create_entry(connection: &mut SqliteConnection, t: chrono::NaiveDateTime, k: f32) {
    let entry = NewEntry { t:t, kg:k};

    diesel::insert_into(mykgs::table)
        .values(&entry)
        .execute(connection)
        .expect("Error inserting new entry");
}

pub fn delete_entry(connection: &mut SqliteConnection, idx: i32) {
    diesel::delete(mykgs::table.filter(mykgs::id.eq(idx)))
        .execute(connection)
        .expect("Error deleting entry");
}


pub fn update_entry(connection: &mut SqliteConnection, idx: i32, t: chrono::NaiveDateTime, k: f32) {
    diesel::update(mykgs::table.filter(mykgs::id.eq(idx)))
        .set((mykgs::t.eq(t), mykgs::kg.eq(k)))
        .execute(connection)
        .expect("Error deleting entry");
}

pub fn query_entries(connection: &mut SqliteConnection) -> Vec<Entry> {
    mykgs::table
        .load::<Entry>(connection)
        .expect("Error loading tasks")
}

