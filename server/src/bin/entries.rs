
#[path = "../db.rs"] mod db;
#[path = "../schema.rs"] mod schema;

use std::env;
use db::{create_entry, query_entries, delete_entry, update_entry, establish_connection};
use chrono::{NaiveDateTime};


fn help() {
    println!("subcommands:");
    println!("    show");
    println!("    new <timestamp, ex: '2012-2-1 7:00:00'> <kg>: create a new task");
    println!("    edit <id> <timestamp, ex: '2012-2-1 7:00:00'> <kg>: edit a task");
    println!("    remove <id>");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 1 {
        help();
        return;
    }

    let subcommand = &args[1];
    match subcommand.as_ref() {
        "new" => new_entry(&args[2..]),
        "edit" => edit_entry(&args[2..]),
        "show" => show_entries(&args[2..]),
        "remove" => remove_entry(&args[2..]),
        _ => help(),
    }
}

fn new_entry(args: &[String]) {
    if args.len() < 2 {
        println!("new: missing  <timestamp, ex: '2012-2-1 7:00:00'> <kg>");
        help();
        return;
    }
    let t = NaiveDateTime::parse_from_str(&args[0], "%Y-%m-%d %H:%M:%S").unwrap() ;
    let k = args[1].parse::<f32>().unwrap();
    let mut conn = establish_connection();
    create_entry(&mut conn, t, k);
}

fn edit_entry(args: &[String]) {
    if args.len() < 3 {
        println!("remove: missing  <id> <timestamp, ex: '2012-2-1 7:00:00'> <kg>");
        help();
        return;
    }
    let id = args[0].parse::<i32>().unwrap();
    let t = NaiveDateTime::parse_from_str(&args[1], "%Y-%m-%d %H:%M:%S").unwrap() ;
    let k = args[2].parse::<f32>().unwrap();
    let mut conn = establish_connection();
    update_entry(&mut conn, id, t, k);
}

fn remove_entry(args: &[String]) {
    if args.len() < 1 {
        println!("remove: missing  <id>");
        help();
        return;
    }
    let id = args[0].parse::<i32>().unwrap();
    let mut conn = establish_connection();
    delete_entry(&mut conn, id);
}

fn show_entries(args: &[String]) {
    if args.len() > 0 {
        println!("show: unexpected argument");
        help();
        return;
    }

    let mut conn = establish_connection();
    println!("ENTRIES\n-----");
    for entry in query_entries(&mut conn) {
        println!("{}: {} {}", entry.id, entry.t, entry.kg);
    }
}

